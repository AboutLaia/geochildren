package com.example.carlos.geochildren.Activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.preference.PreferenceManager;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.carlos.geochildren.Pojo.User;
import com.example.carlos.geochildren.R;
import com.example.carlos.geochildren.Tasks.GetChildrenAsyncTask;
import com.example.carlos.geochildren.Tasks.GetUserAsyncTask;

public class LogInActivity extends AppCompatActivity {

    String username, password;
    EditText etusername, etpassword;
    GetUserAsyncTask task;
    ConnectivityManager manager;
    NetworkInfo info;
    ProgressBar pbLogIn;
    boolean get_again = false;
    SharedPreferences prefs;
    SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_in);

        prefs = PreferenceManager.getDefaultSharedPreferences(this);
        editor = prefs.edit();

        pbLogIn = (ProgressBar) findViewById(R.id.pbLogIn);
        pbLogIn.setVisibility(ProgressBar.INVISIBLE);

        etusername = (EditText) findViewById(R.id.etusername_login);
        etpassword = (EditText) findViewById(R.id.etpassword_login);
    }

    public void buttons_functions(View v) {
        Intent intent = null;
        //Según el botón seleccionado realizamos una acción u otra
        switch (v.getId()) {
            case R.id.blogin: //Enviamos los datos al servidos y comprobamos la existencia del usuario y la carga de sus datos
                if(noneEmpty()){
                    manager = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
                    info = manager.getActiveNetworkInfo();
                    if ((info != null) && info.isConnected()) {
                        task = new GetUserAsyncTask();
                        task.setParent(LogInActivity.this);
                        username = etusername.getText().toString().trim();
                        password = etpassword.getText().toString().trim();
                        String[] params = {username, password};
                        task.execute(params);
                        pbLogIn.setVisibility(ProgressBar.VISIBLE);
                    }
                    else {
                        if (noneEmpty()) {
                            AlertDialog.Builder builder = new AlertDialog.Builder(LogInActivity.this);
                            builder.setTitle(R.string.connection_off_title);
                            builder.setMessage(R.string.connection_off_message);
                            builder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    get_again = true;
                                    Intent intent = new Intent(Settings.ACTION_SETTINGS);
                                    startActivity(intent);
                                }
                            });
                            builder.create().show();
                        }

                    }
                }
                else{
                    Toast.makeText(this, R.string.no_all_paramenters, Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.bregister: //Abre una nueva activity para poder registrarse
                intent = new Intent(LogInActivity.this, RegisterActivity.class);
                this.finish();
                break;
        }
        if (intent != null) {
            startActivity(intent);
        }
    }

    protected void onResume() {
        if (get_again) {
            manager = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
            info = manager.getActiveNetworkInfo();
            if ((info != null) && info.isConnected()) {
                task = new GetUserAsyncTask();
                task.setParent(LogInActivity.this);
                username = etusername.getText().toString().trim();
                password = etpassword.getText().toString().trim();
                String[] params = {username, password};
                task.execute(params);
                pbLogIn.setVisibility(ProgressBar.VISIBLE);
            } else {
                AlertDialog.Builder builder = new AlertDialog.Builder(LogInActivity.this);
                builder.setTitle(R.string.connection_off_title);
                builder.setMessage(R.string.connection_off_message);
                builder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        get_again = true;
                        Intent intent = new Intent(Settings.ACTION_SETTINGS);
                        startActivity(intent);
                    }
                });
                builder.create().show();
            }
        }
        super.onResume();
    }

    public void setUser(User user){
        editor.putString("active_user_username",user.getUsername());
        editor.putString("active_user_name",user.getName());
        editor.putString("active_user_sex",user.getSex());
        editor.putString("active_user_password",user.getPassword());
        if(user.getRol().compareTo("parent") == 0) {
            editor.putBoolean("isParent",true);
        }
        else{
            editor.putBoolean("isParent",false);
        }
        editor.putBoolean("logged",true);
        editor.apply();
        pbLogIn.setVisibility(ProgressBar.INVISIBLE);
        this.finish();
        startActivity(new Intent(this, MainActivity.class));
    }

    public void setInfo(Integer code){
        if(code == 3){
            Toast.makeText(this,R.string.not_username,Toast.LENGTH_SHORT).show();
            pbLogIn.setVisibility(ProgressBar.INVISIBLE);
        }
        else{ //code 4
            Toast.makeText(this,R.string.not_pass,Toast.LENGTH_SHORT).show();
            pbLogIn.setVisibility(ProgressBar.INVISIBLE);
        }
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(this,MainActivity.class));
        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        startActivity(new Intent(this, MainActivity.class));
        return super.onOptionsItemSelected(item);
    }
    public boolean noneEmpty() {
        username = etusername.getText().toString().trim();
        password = etpassword.getText().toString().trim();
        if ((username.compareTo("")) == 0 || (username == null)) {
            etusername.requestFocus();
            return false;
        } else {
            if ((password.compareTo("") == 0) || (password == null)) {
                etpassword.requestFocus();
                return false;
            }

        }
        return true;
    }

}
