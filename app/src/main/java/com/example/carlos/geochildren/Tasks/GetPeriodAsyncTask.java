package com.example.carlos.geochildren.Tasks;

import android.os.AsyncTask;
import android.util.Log;

import com.example.carlos.geochildren.Activities.ChildMapActivity;
import com.example.carlos.geochildren.Activities.MainActivity;
import com.example.carlos.geochildren.Pojo.Position;
import com.example.carlos.geochildren.Services.GeoService;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

/**
 * Created by efrencamarasacarreres on 23/4/16.
 */
public class GetPeriodAsyncTask extends AsyncTask<String, Void, Integer> {

    GeoService parent;
    Integer code;
    Integer result = 30;
    String task = "GetPeriodAsyncTask";

    public static String convertStreamToString(java.io.InputStream is) {
        java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
        return s.hasNext() ? s.next() : "";
    }

    public void setParent(GeoService parent) {
        this.parent = parent;
    }

    @Override
    protected Integer doInBackground(String... params) {

        InputStreamReader isr = null;
        HttpURLConnection connection = null;

        try {
            //URL url = new URL(builder.build().toString());
            URL url = new URL("http://213.201.97.154:8081/gc/v1/prefs/" + params[0] + "/" + params[1]);
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.setDoInput(true);
            if (connection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                isr = new InputStreamReader(connection.getInputStream());

                String response = convertStreamToString(connection.getInputStream());
                Log.i("response", response);
                JSONObject object = null;
                code = -1;
                String message = null;
                try {
                    object = new JSONObject(response);
                    code = object.getInt("code");
                    message = object.getString("message");
                    if (code == 6){
                        result = 30;
                    }

                    if (code == 3) {
                        Log.i(task, "El username no existe");
                    }

                    if (code == 4) {
                        Log.i(task, "La contraseña no es correcta");
                    }

                    if (code == 0) {
                        result = object.getInt("data");
                        Log.i("Period",""+result);
                    }
                } catch (JsonSyntaxException | JsonIOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } /*finally {
            try {
                isr.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            connection.disconnect();
        }*/

        return result;
    }

    @Override
    protected void onPostExecute(Integer result) {
        parent.setPeriod(result);
        super.onPostExecute(result);
    }
}
