package com.example.carlos.geochildren.Services;

import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v7.preference.PreferenceManager;
import android.util.Log;

import com.example.carlos.geochildren.R;
import com.example.carlos.geochildren.Tasks.GetPeriodAsyncTask;
import com.example.carlos.geochildren.Tasks.PostActualPositionAsyncTask;

import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;
import java.util.regex.Pattern;

public class GeoService extends Service {


    int period = 5*60*1000; //5 minutos
    int period_loc = 5*60*1000; //5 minutos
    String [] params;
    GetPeriod get_period;
    //LocationsTask get_location;
    Timer mytimer;
    SharedPreferences prefs;
    LocationManager mLocationManager;
    ConnectivityManager manager;
    NetworkInfo info;
    String street = "";
    String province = "";

    private final LocationListener mLocationListener = new LocationListener() {
        @Override
        public void onLocationChanged(final Location location) {
            //your code here
            manager = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
            info = manager.getActiveNetworkInfo();
            if ((info != null) && info.isConnected()) {
                Log.i("Method", "Entra en onLocationChanged");
                PostActualPositionAsyncTask task = new PostActualPositionAsyncTask();
                prefs = PreferenceManager.getDefaultSharedPreferences(GeoService.this);
                String username = prefs.getString("active_user_username", "No Username");
                String pass = prefs.getString("active_user_password", "No Password").replace("$", "%24");
                String latitude = String.valueOf(location.getLatitude());
                String longitude = String.valueOf(location.getLongitude());
                Geocoder gc = new Geocoder(GeoService.this, Locale.getDefault());
                List<Address> addresses = null;
                try {
                    addresses = gc.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
                    Log.i("addresses",addresses.toString());
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Address ad = null;
                if ((addresses != null) && (addresses.size() > 0)) {
                    ad = addresses.get(0);
                    int addressLines = ad.getMaxAddressLineIndex();
                    if (addressLines != -1) {
                        street = ad.getAddressLine(0);
                        Log.i("street get",street);
                        if (addressLines > 1) {
                            province = province + "" + ad.getAddressLine(1);
                            for (int i = 2; i <= addressLines; i++) {
                                province = province + ", " + ad.getAddressLine(i);
                            }
                        }
                    } else {
                        street = getResources().getString(R.string.geocoder_not_available);
                        province = getResources().getString(R.string.geocoder_not_available);
                    }
                }
                else{
                    street = getResources().getString(R.string.geocoder_not_available);
                    province = getResources().getString(R.string.geocoder_not_available);
                }

                Pattern spaces = Pattern.compile("\\s+");

                Log.i("username",username);
                Log.i("pass",pass);
                Log.i("latitud",latitude);
                Log.i("longitud",longitude);
                street = spaces.matcher(street.trim()).replaceAll("%20");
                province = spaces.matcher(province.trim()).replaceAll("%20");
                Log.i("street",street);
                Log.i("province",province);
                String[] params = {username, pass, latitude, longitude, street, province};
                task.execute(params);
                street = "";
                province = "";
            }
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {

        }

        @Override
        public void onProviderEnabled(String provider) {
            Log.i(provider,"Habilitat");
        }

        @Override
        public void onProviderDisabled(String provider) {
            Log.i(provider,"Deshabilitat");
        }
    };

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i("StateService","Comença el service");
        //Toast.makeText(this,"Service Started", Toast.LENGTH_LONG).show();
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        Log.i("StateService","Acaba el service");
        //Toast.makeText(this,"Service Finished",Toast.LENGTH_LONG).show();
        super.onDestroy();
    }

    @Override
    public boolean stopService(Intent name) {
        stopSelf();
        Log.i("StateService","Stop Service");
        return super.stopService(name);
    }

    @Override
    public void onCreate() {
        Log.i("StateService","Creat el service");
        //Toast.makeText(this,"Service Created",Toast.LENGTH_LONG).show();
        mytimer = new Timer();
        get_period = new GetPeriod();
        mytimer.scheduleAtFixedRate(get_period,0,period);

        mLocationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        if(mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)){
            mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,period_loc,10,mLocationListener);
        }

        super.onCreate();
    }

    public void setPeriod(Integer result){
        if (result != period_loc) {
            period_loc = result*60*1000; //A milisegundos
            Log.i("setPeriod",""+period_loc);
            mLocationManager.removeUpdates(mLocationListener);
            mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,period_loc,10,mLocationListener);
        }
        //mytimer.cancel();
        //get_location = new LocationsTask();
        //mytimer.scheduleAtFixedRate(get_location,0,period);
    }

    private class GetPeriod extends TimerTask{

        @Override
        public void run() {
            //Toast.makeText(GeoService.this,"Timer Inicialized",Toast.LENGTH_LONG).show();
            Log.i("Run","run inicialized");
            prefs = PreferenceManager.getDefaultSharedPreferences(GeoService.this);
            GetPeriodAsyncTask task = new GetPeriodAsyncTask();
            task.setParent(GeoService.this);
            params = new String[2];
            params[0] = prefs.getString("active_user_username","No Username");
            params[1] = prefs.getString("active_user_password","No Password").replace("$","%24");
            task.execute(params);
        }
    }
/*
    private class LocationsTask extends TimerTask {

        @Override
        public void run() {
            Intent intent = new Intent(GeoService.this,LocationActivity.class);
            intent.putExtra("period",period);
            startActivity(intent);
        }
    }*/
}
