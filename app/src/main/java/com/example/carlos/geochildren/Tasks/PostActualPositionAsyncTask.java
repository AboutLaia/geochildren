package com.example.carlos.geochildren.Tasks;

import android.net.Uri;
import android.os.AsyncTask;

import com.example.carlos.geochildren.Activities.ChildMapActivity;
import com.example.carlos.geochildren.Pojo.Position;
import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

/**
 * Created by efrencamarasacarreres on 22/4/16.
 */
public class PostActualPositionAsyncTask extends AsyncTask<String,Void,Void> {
    //Para que un niño envie su ubicación actual al servidor
    ChildMapActivity parent;
    String username,pass,longitude,latitude,street,province;
    Position position_children;

    @Override
    protected Void doInBackground(String... params) {

        username = params[0];
        pass = params[1];
        latitude = params[2];
        longitude = params[3];
        street = params[4];
        province = params[5];

        InputStreamReader isr = null;
        HttpURLConnection connection = null;
        /*
        Uri.Builder builder = new Uri.Builder();
        builder.scheme("http");
        builder.authority("213.201.97.154:8081");
        builder.appendPath("ps");
        builder.appendPath("v1");
        builder.appendPath("users");
        builder.appendPath("position");
        builder.appendPath(child_username);
        */

        try {
            //URL url = new URL(builder.build().toString());
            URL url = new URL("http://213.201.97.154:8081/gc/v1/locations/"+username+"/"+pass+"/"+latitude+
            "/"+longitude+"/"+street+"/"+province);
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setDoInput(true);
            if (connection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                //isr = new InputStreamReader(connection.getInputStream());

                try {

                } catch (JsonSyntaxException | JsonIOException e) {
                    e.printStackTrace();
                }
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        /*finally {
            try {
                isr.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            connection.disconnect();
        }*/

        return null;



    }
}
