package com.example.carlos.geochildren.Activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Address;
import android.location.Geocoder;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.provider.Settings;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.preference.PreferenceManager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.carlos.geochildren.Pojo.Position;
import com.example.carlos.geochildren.R;
import com.example.carlos.geochildren.Tasks.GetActualPostionAsyncTask;
import com.example.carlos.geochildren.Tasks.PostUserAsyncTask;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.util.List;

public class ChildMapActivity extends AppCompatActivity implements OnMapReadyCallback, GoogleMap.OnInfoWindowClickListener{

    GoogleMap map;
    Geocoder geocoder;
    String name;
    SharedPreferences prefs;
    SharedPreferences.Editor editor;
    MyInfoWindowAdapter infoWindowAdapter;
    ConnectivityManager manager;
    NetworkInfo info;
    GetActualPostionAsyncTask task;
    boolean get_again = false;
    ProgressBar pbMap;
    double lat, longi;
    String date;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_child_map);

        prefs = PreferenceManager.getDefaultSharedPreferences(this);
        editor = prefs.edit();

        pbMap = (ProgressBar) findViewById(R.id.pbMap);
        pbMap.setVisibility(View.INVISIBLE);

        name = prefs.getString("active_child_name","No Name");
        getSupportActionBar().setTitle(getString(R.string.pos)+" "+name);

        geocoder = new Geocoder(this);

        infoWindowAdapter = new MyInfoWindowAdapter();

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.mapChild);
        mapFragment.getMapAsync(this);

        manager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        info = manager.getActiveNetworkInfo();

        if ((info != null) && info.isConnected()) {
            //pbRegister.setVisibility(ProgressBar.VISIBLE);
            task = new GetActualPostionAsyncTask();
            task.setParent(ChildMapActivity.this);
            String user_username = prefs.getString("active_user_username","No Username");
            String user_pass = prefs.getString("active_user_password","No Username").replace("$","%24");
            String child_username = prefs.getString("active_child_username","No Username");
            String[] params = {user_username, user_pass, child_username};
            task.execute(params);
            pbMap.setVisibility(View.VISIBLE);
        } else {
            AlertDialog.Builder builder = new AlertDialog.Builder(ChildMapActivity.this);
            builder.setTitle(R.string.connection_off_title);
            builder.setMessage(R.string.connection_off_message);
            builder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    get_again = true;
                    Intent intent = new Intent(Settings.ACTION_SETTINGS);
                    startActivity(intent);
                }
            });
            builder.create().show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (map != null) {
            getMenuInflater().inflate(R.menu.map_menu, menu);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.mNormalMap:
                map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                return true;
            case R.id.mTerrainMap:
                map.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
                return true;
            case R.id.mSatelliteMap:
                map.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
                return true;
        }
        return super.onOptionsItemSelected(item); //Flecha hacia atrás
    }

    @Override
    public void onInfoWindowClick(Marker marker) {

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        map.setInfoWindowAdapter(infoWindowAdapter);
        map.setOnInfoWindowClickListener(this);
        supportInvalidateOptionsMenu();
        pbMap.setVisibility(View.VISIBLE);
    }

    private class MyInfoWindowAdapter implements GoogleMap.InfoWindowAdapter {

        @Override
        public View getInfoWindow(Marker marker) {
            return null;
        }

        @Override
        public View getInfoContents(Marker marker) {
            View result = getLayoutInflater().inflate(R.layout.custom_info_window, null);
            TextView tvTitle = (TextView) result.findViewById(R.id.tvTitle);
            TextView tvSnippet = (TextView) result.findViewById(R.id.tvSnippet);
            TextView tvDate = (TextView) result.findViewById(R.id.tvDate);
            tvTitle.setText(marker.getTitle());
            tvSnippet.setText(marker.getSnippet());
            String [] ar = date.split("T");
            Log.i("ar",ar[0]+" "+ar[1]);
            String [] ar2 = ar[1].split("\\.");
            Log.i("ar2",ar2[0]);
            tvDate.setText(ar[0]+" "+ar2[0]+" (GMT)");

            return result;
        }
    }

    private void addMarker(double latitude, double longitude, String title, String snippet, float color) {
        MarkerOptions options = new MarkerOptions();
        options.position(new LatLng(latitude, longitude));
        options.title(title);
        options.snippet(snippet);
        options.icon(BitmapDescriptorFactory.defaultMarker(color));

        map.addMarker(options);
    }

    public void setActualPos(Position pos){
        addMarker(Double.valueOf(pos.getLatitud()),Double.valueOf(pos.getLongitud()),pos.getStreet(),
                pos.getCity_country(), BitmapDescriptorFactory.HUE_RED);
        lat = Double.valueOf(pos.getLatitud());
        Log.i("lat",""+lat);
        longi = Double.valueOf(pos.getLongitud());
        Log.i("longi",""+longi);
        date = pos.getDate();
        map.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lat, longi), 10));
        pbMap.setVisibility(View.INVISIBLE);
    }

    @Override
    protected void onResume() {
        if (get_again) {
            manager = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
            info = manager.getActiveNetworkInfo();
            if ((info != null) && info.isConnected()) {
                task = new GetActualPostionAsyncTask();
                task.setParent(ChildMapActivity.this);
                String user_username = prefs.getString("active_user_username","No Username");
                String user_pass = prefs.getString("active_user_password","No Password").replace("$","%24");
                String child_username = prefs.getString("active_child_username","No Username");
                String[] params = {user_username, user_pass, child_username};
                task.execute(params);
                pbMap.setVisibility(View.VISIBLE);
            } else {
                AlertDialog.Builder builder = new AlertDialog.Builder(ChildMapActivity.this);
                builder.setTitle(R.string.connection_off_title);
                builder.setMessage(R.string.connection_off_message);
                builder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        get_again = true;
                        Intent intent = new Intent(Settings.ACTION_SETTINGS);
                        startActivity(intent);
                    }
                });
                builder.create().show();
            }
        }
        super.onResume();
    }

    public void setNoPos(){
        Toast.makeText(this,R.string.no_positions_yet,Toast.LENGTH_SHORT).show();
        pbMap.setVisibility(View.INVISIBLE);
    }
}
