package com.example.carlos.geochildren.Activities;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.preference.PreferenceManager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.example.carlos.geochildren.R;
import com.example.carlos.geochildren.Services.GeoService;
import com.example.carlos.geochildren.Tasks.GetChildrenAsyncTask;
import com.example.carlos.geochildren.Tasks.GetParentsAsyncTask;
import com.example.carlos.geochildren.Tasks.PutChildAsyncTask;
import com.example.carlos.geochildren.Tasks.PutDeleteChildAsyncTask;

import java.util.ArrayList;
import java.util.HashMap;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    Toolbar toolBar;
    DrawerLayout drawerLayout;
    ActionBarDrawerToggle toggle;
    SharedPreferences prefs;
    SharedPreferences.Editor editor;
    String active_user_na;
    String active_user_usna;
    String active_user_sex;
    String name;
    TextView active_user_name;
    TextView active_user_username;
    ImageView active_user_photo;
    ListView lv;
    ArrayList<HashMap<String, Object>> childrens;
    Boolean isParent;
    GetChildrenAsyncTask task;
    GetParentsAsyncTask task2;
    PutChildAsyncTask task3;
    PutDeleteChildAsyncTask task4;
    boolean get_again = false;
    boolean get_again2 = false;
    boolean get_again3 = false;
    boolean get_again4 = false;
    ConnectivityManager manager;
    NetworkInfo info;
    ProgressBar pbMain;
    boolean first_time;
    boolean log;
    SimpleAdapter adapter;
    String name_child;
    Intent service = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        prefs = PreferenceManager.getDefaultSharedPreferences(this);
        editor = prefs.edit();

        first_time = prefs.getBoolean("first_time", true);

        //Si es la primera vez que se habre la aplicación se muestra la pantalla de bienvenida
        if (first_time) {
            Intent intent = new Intent (this,WelcomeActivity.class);
            if(intent != null){
                this.finish();
                startActivity(intent);
            }
        } else { //Sino, se muestra la lista de children/parents

            isParent = prefs.getBoolean("isParent", true);
            log = prefs.getBoolean("logged", false);
            pbMain = (ProgressBar) findViewById(R.id.pbMain);
            pbMain.setVisibility(ProgressBar.INVISIBLE);
            childrens = new ArrayList<>();

            //FloatingActionButton
            FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fabMessage);
            //Listener para el FloatingActionButton
            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //Muestra un cuadro de dialogo para intriducir el nombre del children
                    final AlertDialog.Builder add_child = new AlertDialog.Builder(MainActivity.this);
                    add_child.setTitle(R.string.add_child_title);
                    add_child.setMessage(R.string.add_child_message);
                    final EditText user_child = new EditText(MainActivity.this);
                    user_child.setHint(R.string.hint_username_child);
                    add_child.setView(user_child);

                    add_child.setPositiveButton(R.string.accept_add_child, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Log.i("Abans if", user_child.getText().toString());
                            if((!user_child.getText().toString().trim().matches("")) ) {
                                Log.i("hola", user_child.getText().toString());
                                if ((info != null) && info.isConnected()) {
                                    //Toast.makeText(MainActivity.this, R.string.petition_child, Toast.LENGTH_SHORT).show();
                                    name_child = user_child.getText().toString().trim();
                                    String username_put_child = prefs.getString("active_user_username", "No Name");
                                    String password_put_child = prefs.getString("active_user_password", "No Password").replace("$", "%24");
                                    task3 = new PutChildAsyncTask();
                                    task3.setParent(MainActivity.this);
                                    String[] params = {username_put_child, password_put_child, name_child};
                                    task3.execute(params);
                                    pbMain.setVisibility(ProgressBar.VISIBLE);
                                } else {
                                    AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                                    builder.setTitle(R.string.connection_off_title);
                                    builder.setMessage(R.string.connection_off_message);
                                    builder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                                get_again3 = true;
                                                Intent intent = new Intent(Settings.ACTION_SETTINGS);
                                                startActivity(intent);
                                            }

                                    });
                                    builder.create().show();
                                }
                            }
                            else{
                                Toast.makeText(MainActivity.this,"Empty name field!",Toast.LENGTH_LONG).show();
                            }
                        }
                    });
                    add_child.setNegativeButton(R.string.cancel_add_child, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
                    add_child.create().show();
                }
            });

            //Si es un parent aparaece el boton para añadir mas niños, si es un niño no
            if(log) {
                if (!isParent) {
                    fab.setVisibility(View.INVISIBLE);
                } else {
                    fab.setVisibility(View.VISIBLE);
                }
            }
            else{
                fab.setVisibility(View.INVISIBLE);
            }

            //Toolbar personalizado
            toolBar = (Toolbar) findViewById(R.id.toolBar);
            setSupportActionBar(toolBar);
            drawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout_main);
            toggle = new ActionBarDrawerToggle(this, drawerLayout, toolBar,
                    R.string.navigation_drawer_open, R.string.navigation_drawer_close);
            drawerLayout.addDrawerListener(toggle);
            toggle.syncState();

            //Barra lateral
            NavigationView navigationView = (NavigationView) findViewById(R.id.navView);
            navigationView.setNavigationItemSelectedListener(this);
            //drawerLayout.openDrawer(GravityCompat.START); //Abre directamente el navigation view al arrancar la app

            //Si hay una sesión iniciada, aparece Sign out, sino aparece Sign in
            Menu nvMenu = navigationView.getMenu();
            MenuItem sign_in_out = nvMenu.findItem(R.id.mInicioSesionItem);
            active_user_na = prefs.getString("active_user_name", "No Name");
            active_user_usna = prefs.getString("active_user_username", "No Username");

            if (active_user_na.compareTo("No Name") == 0) {
                sign_in_out.setTitle(R.string.title_sign_in_fragment);
                editor.putBoolean("logged", false);
                editor.apply();
            } else {
                sign_in_out.setTitle(R.string.title_sign_out_fragment);
                editor.putBoolean("logged", true);
                editor.apply();
            }

            View navHeaderView = navigationView.inflateHeaderView(R.layout.header_navigation_drawer); //Esta linea corresponde a app:headerLayout="@layout/header_navigation_drawer" en NavigationView de activity_main.xml
            //Cambia los nombres del header del NavigationView por los del usuario activo
            active_user_name = (TextView) navHeaderView.findViewById(R.id.parent_name_header_drawer);
            active_user_username = (TextView) navHeaderView.findViewById(R.id.parent_username_header_drawer);
            active_user_name.setText(active_user_na);
            active_user_username.setText(active_user_usna);

            //Dependiendo del sexo, se muestra una imagen u otra
            active_user_sex = prefs.getString("active_user_sex", "No Sex");
            active_user_photo = (ImageView) navHeaderView.findViewById(R.id.parent_photo_header_drawer);
            if (isParent) {
                if (log) {
                    if (active_user_sex.compareTo("m") == 0) { //Si es un chico
                        active_user_photo.setImageResource(R.drawable.father);
                    } else { //Si es una chica
                        active_user_photo.setImageResource(R.drawable.mother);
                    }
                } else { //No está logueado
                    active_user_photo.setImageResource(R.drawable.change_image);
                }
            } else {
                if (log) {
                    if (active_user_sex.compareTo("m") == 0) { //Si es un chico
                        active_user_photo.setImageResource(R.drawable.son);
                    } else { //Si es una chica
                        active_user_photo.setImageResource(R.drawable.daughter);
                    }
                } else { //No está logueado
                    active_user_photo.setImageResource(R.drawable.change_image);
                }
            }

            lv = (ListView) findViewById(R.id.listViewChildren);
            adapter = new SimpleAdapter(this, childrens, R.layout.children_rectangle, new String[]{"name", "username", "sex_image", "sex"}, new int[]{R.id.children_name_rectangle, R.id.children_username_rectangle, R.id.children_image_rectangle, R.id.children_sex_rectangle});
            lv.setAdapter(adapter);

            Log.i("log",""+log);
            if (log) { //Si está la sesión iniciada, cargamos la lista
                if (isParent) { //Si es un parent, cargamos la lista de los niños
                    manager = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
                    info = manager.getActiveNetworkInfo();
                    if ((info != null) && info.isConnected()) {
                        task = new GetChildrenAsyncTask();
                        task.setParent(MainActivity.this);
                        String username_get_children = prefs.getString("active_user_username", "No Name");
                        String password_get_children = prefs.getString("active_user_password", "No Password").replace("$","%24");
                        String[] params = {username_get_children, password_get_children};
                        task.execute(params);
                        pbMain.setVisibility(ProgressBar.VISIBLE);
                    } else {
                        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                        builder.setTitle(R.string.connection_off_title);
                        builder.setMessage(R.string.connection_off_message);
                        builder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                get_again = true;
                                Intent intent = new Intent(Settings.ACTION_SETTINGS);
                                startActivity(intent);
                            }
                        });
                        builder.create().show();
                    }
                } else { //Si es un niño, cargamos la lista de los parents
                    manager = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
                    info = manager.getActiveNetworkInfo();
                    if ((info != null) && info.isConnected()) {
                        task2 = new GetParentsAsyncTask();
                        task2.setParent(MainActivity.this);
                        String username_get_parents = prefs.getString("active_user_username", "No Name");
                        String password_get_parents = prefs.getString("active_user_password", "No Password").replace("$","%24");
                        String[] params = {username_get_parents, password_get_parents};
                        task2.execute(params);
                        pbMain.setVisibility(ProgressBar.VISIBLE);
                    } else {
                        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                        builder.setTitle(R.string.connection_off_title);
                        builder.setMessage(R.string.connection_off_message);
                        builder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                get_again2 = true;
                                Intent intent = new Intent(Settings.ACTION_SETTINGS);
                                startActivity(intent);
                            }
                        });
                        builder.create().show();
                    }
                }
            }

            //Si es un padre puede ver el menú de cada child..., un child no
            if (isParent) {
                lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                        Intent intent = new Intent(MainActivity.this, MenuChildrenActivity.class);
                        editor.putString("active_child_name", ((TextView) view.findViewById(R.id.children_name_rectangle)).getText().toString());
                        editor.putString("active_child_username", ((TextView) view.findViewById(R.id.children_username_rectangle)).getText().toString());
                        editor.putString("active_child_sex", ((TextView) view.findViewById(R.id.children_sex_rectangle)).getText().toString());
                        editor.apply();
                        startActivity(intent);
                    }
                });
            }

            //Si es un padre puede eliminar un child..., un child no puede eliminar paarents
            if (isParent) {
                lv.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                    @Override
                    public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id) {
                        //Muestra un cuadro de dialogo para eliminar el child
                        final AlertDialog.Builder add_child = new AlertDialog.Builder(MainActivity.this);
                        add_child.setTitle(getString(R.string.delete_child_title) + " " + ((TextView) view.findViewById(R.id.children_name_rectangle)).getText().toString()); //¿Mostar el nombre o el nombre de usuario?
                        add_child.setMessage("¿" + getString(R.string.delete_child_message) + " " + ((TextView) view.findViewById(R.id.children_username_rectangle)).getText().toString() + "?"); //¿Mostar el nombre o el nombre de usuario?

                        final String name_child = ((TextView) view.findViewById(R.id.children_username_rectangle)).getText().toString();

                        add_child.setPositiveButton(R.string.delete_child, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                manager = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
                                info = manager.getActiveNetworkInfo();
                                if ((info != null) && info.isConnected()) {
                                    task4 = new PutDeleteChildAsyncTask();
                                    task4.setParent(MainActivity.this);
                                    String username_delete_child = prefs.getString("active_user_username", "No Name");
                                    String password_delete_child = prefs.getString("active_user_password", "No Password").replace("$","%24");
                                    String[] params = {username_delete_child, password_delete_child, name_child};
                                    task4.execute(params);
                                    pbMain.setVisibility(ProgressBar.VISIBLE);
                                } else {
                                    AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                                    builder.setTitle(R.string.connection_off_title);
                                    builder.setMessage(R.string.connection_off_message);
                                    builder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            get_again4 = true;
                                            Intent intent = new Intent(Settings.ACTION_SETTINGS);
                                            startActivity(intent);
                                        }
                                    });
                                    builder.create().show();
                                }

                            }
                        });
                        add_child.setNegativeButton(R.string.cancel_delete_child, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });
                        add_child.create().show();
                        return true;
                    }
                });
            }

            //Si no es padre y está la sesión iniciada, lanza el service que nos da la localizacion
            if (!isParent && log) {
                //startService(new Intent(this,GeoService.class));
                schedule();
            }
        }
    }

    public void schedule(){

        int checkWriteablePermission =
                ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);
        if (PackageManager.PERMISSION_GRANTED == checkWriteablePermission) {
            service = new Intent(this,GeoService.class);
            startService(service);
        } else {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 0);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == 0){
            if ((grantResults.length > 0) && (PackageManager.PERMISSION_GRANTED == grantResults[0])){
                service = new Intent(this,GeoService.class);
                startService(service);
            }
        }
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        Fragment fragment = null;
        String tag = null;
        Intent intent = null;
        Boolean logged;
        prefs = PreferenceManager.getDefaultSharedPreferences(this);
        editor = prefs.edit();

        // Determine the action to take place according to the Id of the action selected
        switch (item.getItemId()) {

            case R.id.mInicioSesionItem:
                logged = prefs.getBoolean("logged", false);
                if (!logged) {//Sign in
                    intent = new Intent(this, LogInActivity.class);
                } else {//Sign out
                    editor.putString("active_user_name", "No Name");
                    editor.putString("active_user_username", "No Username");
                    editor.putString("active_user_sex", "No Sex");
                    editor.putBoolean("logged", false);
                    editor.apply();
                    if(service != null){
                        Log.i("Service",service.toString());
                        stopService(service);
                    }
                    recreate();
                }
                break;
            // Si se pulsa Ajustes
            case R.id.mAjustesItem:
                logged = prefs.getBoolean("logged", false);
                if(logged) {
                    intent = new Intent(MainActivity.this, PreferencesActivity.class);
                }
                else{
                    Toast.makeText(this,R.string.no_session,Toast.LENGTH_SHORT).show();
                }
                break;

            // Si se pulsa Guía para Padres
            case R.id.mGuiaPadresItem:
                intent = new Intent(MainActivity.this, GuideParentsActivity.class);
                break;

            // Si se pulsa Guía para hijos
            case R.id.mGuiaHijosItem:
                intent = new Intent(MainActivity.this, GuideChildrenActivity.class);
                break;

            // Display GridViewFragment
            case R.id.mAcercadeItem:
                intent = new Intent(MainActivity.this, AboutActivity.class);
                break;
            default:
                super.onOptionsItemSelected(item);
                break;
        }
        if (intent != null) {
            this.finish();
            startActivity(intent);
        }

        drawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    protected void onResume() {
        if (get_again) {
            manager = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
            info = manager.getActiveNetworkInfo();
            if ((info != null) && info.isConnected()) {
                task = new GetChildrenAsyncTask();
                task.setParent(MainActivity.this);
                String[] params = {prefs.getString("active_user_username", "No Name"), prefs.getString("active_user_password", "No Password")};
                get_again = false;
                task.execute(params);
                pbMain.setVisibility(ProgressBar.VISIBLE);
            } else {
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setTitle(R.string.connection_off_title);
                builder.setMessage(R.string.connection_off_message);
                builder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        get_again = true;
                        Intent intent = new Intent(Settings.ACTION_SETTINGS);
                        startActivity(intent);
                    }
                });
                builder.create().show();
            }
        }

        if (get_again2) {
            manager = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
            info = manager.getActiveNetworkInfo();
            if ((info != null) && info.isConnected()) {
                task2 = new GetParentsAsyncTask();
                task2.setParent(MainActivity.this);
                String[] params = {prefs.getString("active_user_username", "No Name"), prefs.getString("active_user_password", "No Password")};
                get_again2 = false;
                task2.execute(params);
                pbMain.setVisibility(ProgressBar.VISIBLE);
            } else {
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setTitle(R.string.connection_off_title);
                builder.setMessage(R.string.connection_off_message);
                builder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        get_again2 = true;
                        Intent intent = new Intent(Settings.ACTION_SETTINGS);
                        startActivity(intent);
                    }
                });
                builder.create().show();
            }
        }

        if (get_again3) { //Corregir
            manager = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
            info = manager.getActiveNetworkInfo();
            if ((info != null) && info.isConnected()) {
                //pbScores.setVisibility(ProgressBar.VISIBLE);
                task3 = new PutChildAsyncTask();
                task3.setParent(MainActivity.this);
                String[] params = {prefs.getString("active_user_username", "No Name"), prefs.getString("active_user_password", "No Password")};
                get_again2 = false;
                //task2.execute(params);
                task3.execute(params);
            } else {
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setTitle(R.string.connection_off_title);
                builder.setMessage(R.string.connection_off_message);
                builder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        get_again2 = true;
                        Intent intent = new Intent(Settings.ACTION_SETTINGS);
                        startActivity(intent);
                    }
                });
                builder.create().show();
            }
        }

        //get_again4
        super.onResume();
    }

    public void setListChildren(ArrayList<HashMap<String, Object>> result) { //Para GetChildrenAsyncTask, PutDeleteChildAsyncTask y PutChildAsyncTask
       // childrens.clear();
        childrens.clear();
        childrens.addAll(result);
        for( HashMap<String, Object> aux : result) {
            if(aux.get("sex").equals("m")){
                aux.put("sex_image",R.drawable.son);
                adapter.notifyDataSetChanged();
            }
            else{
               aux.put("sex_image",R.drawable.daughter);
                adapter.notifyDataSetChanged();
            }
        }
        adapter.notifyDataSetChanged();

        //childrens = result;
        //adapter = new SimpleAdapter(this, childrens, R.layout.children_rectangle, new String[]{"name", "username", "sex_image", "sex"}, new int[]{R.id.children_name_rectangle, R.id.children_username_rectangle, R.id.children_image_rectangle, R.id.children_sex_rectangle});
        //lv.setAdapter(adapter);
        pbMain.setVisibility(ProgressBar.INVISIBLE);
    }

    public void setListParents(ArrayList<HashMap<String, Object>> result) { //Para GetParentsAsyncTask
        childrens.clear();
        childrens.addAll(result);
        adapter.notifyDataSetChanged();

        //adapter = new SimpleAdapter(this, childrens, R.layout.children_rectangle, new String[]{"name", "username", "sex_image"}, new int[]{R.id.children_name_rectangle, R.id.children_username_rectangle, R.id.children_image_rectangle});
        //lv.setAdapter(adapter);
        pbMain.setVisibility(ProgressBar.INVISIBLE);
    }

    public void setInfoChildren(Integer code){
        if(code == 6){
            Toast.makeText(this,R.string.childrens_not_found,Toast.LENGTH_SHORT).show();
            pbMain.setVisibility(ProgressBar.INVISIBLE);
        }
    }

    public void setInfoParents(Integer code){
        if(code == 6){
            Toast.makeText(this,R.string.parents_not_found,Toast.LENGTH_SHORT).show();
            pbMain.setVisibility(ProgressBar.INVISIBLE);
        }
    }

    public void setInfoAddChild(Integer code){
        if(code == 3){
            Toast.makeText(this,R.string.child_username_not_found,Toast.LENGTH_SHORT).show();
            pbMain.setVisibility(ProgressBar.INVISIBLE);
        }
    }

}