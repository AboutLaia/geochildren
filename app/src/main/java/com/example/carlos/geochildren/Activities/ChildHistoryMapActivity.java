package com.example.carlos.geochildren.Activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.preference.PreferenceManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import com.example.carlos.geochildren.Pojo.Position;
import com.example.carlos.geochildren.R;
import com.example.carlos.geochildren.Tasks.GetActualPostionAsyncTask;
import com.example.carlos.geochildren.Tasks.GetHistoryPositionAsyncTask;
import com.google.android.gms.maps.GoogleMap;

import java.util.ArrayList;
import java.util.HashMap;

public class ChildHistoryMapActivity extends AppCompatActivity {

    String name;
    SharedPreferences prefs;
    SharedPreferences.Editor editor;

    ListView lv;
    ArrayList<HashMap<String,String>> positions;
    SimpleAdapter adapter;

    ProgressBar pb;
    boolean get_again = false;
    boolean get_again2 = false;
    boolean get_again3 = false;
    GetHistoryPositionAsyncTask task;
    ConnectivityManager manager;
    NetworkInfo info;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_child_history_map);
        positions = new ArrayList<>();
        adapter = new SimpleAdapter(this, positions, R.layout.label_history_children, new String[]{"street", "province", "date"}, new int[]{R.id.street, R.id.city_country, R.id.date});

        prefs = PreferenceManager.getDefaultSharedPreferences(this);
        editor = prefs.edit();

        pb = (ProgressBar) findViewById(R.id.pbHistoryPosition);
        pb.setVisibility(View.INVISIBLE);

        name = prefs.getString("active_child_name","No Name");
        getSupportActionBar().setTitle(getString(R.string.historial)+" "+name);

        lv = (ListView) findViewById(R.id.listViewHistory);
        lv.setAdapter(adapter);


        manager = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
        info = manager.getActiveNetworkInfo();
        if ((info != null) && info.isConnected()) {
            task = new GetHistoryPositionAsyncTask();
            task.setParent(ChildHistoryMapActivity.this);
            String user_username = prefs.getString("active_user_username","No Username");
            String user_pass = prefs.getString("active_user_password","No Password").replace("$","%24");
            String child_username = prefs.getString("active_child_username","No Username");
            String period = "ld";
            String[] params = {user_username, user_pass, child_username, period};
            task.execute(params);
            pb.setVisibility(View.VISIBLE);
        } else {
            AlertDialog.Builder builder = new AlertDialog.Builder(ChildHistoryMapActivity.this);
            builder.setTitle(R.string.connection_off_title);
            builder.setMessage(R.string.connection_off_message);
            builder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    get_again = true;
                    Intent intent = new Intent(Settings.ACTION_SETTINGS);
                    startActivity(intent);
                }
            });
            builder.create().show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.history_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.mToday: //Historial de hoy
                manager = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
                info = manager.getActiveNetworkInfo();
                if ((info != null) && info.isConnected()) {
                    task = new GetHistoryPositionAsyncTask();
                    task.setParent(ChildHistoryMapActivity.this);
                    String user_username = prefs.getString("active_user_username","No Username");
                    String user_pass = prefs.getString("active_user_password","No Password").replace("$","%24");
                    String child_username = prefs.getString("active_child_username","No Username");
                    String period = "ld";
                    String[] params = {user_username, user_pass, child_username, period};
                    task.execute(params);
                    pb.setVisibility(View.VISIBLE);
                } else {
                    AlertDialog.Builder builder = new AlertDialog.Builder(ChildHistoryMapActivity.this);
                    builder.setTitle(R.string.connection_off_title);
                    builder.setMessage(R.string.connection_off_message);
                    builder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            get_again = true;
                            Intent intent = new Intent(Settings.ACTION_SETTINGS);
                            startActivity(intent);
                        }
                    });
                    builder.create().show();
                }
                return true;
            case R.id.mWeek: //Historial de la última semana
                manager = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
                info = manager.getActiveNetworkInfo();
                if ((info != null) && info.isConnected()) {
                    task = new GetHistoryPositionAsyncTask();
                    task.setParent(ChildHistoryMapActivity.this);
                    String user_username = prefs.getString("active_user_username","No Username");
                    String user_pass = prefs.getString("active_user_password","No Password").replace("$","%24");
                    String child_username = prefs.getString("active_child_username","No Username");
                    String period = "lw";
                    String[] params = {user_username, user_pass, child_username, period};
                    task.execute(params);
                    pb.setVisibility(View.VISIBLE);
                } else {
                    AlertDialog.Builder builder = new AlertDialog.Builder(ChildHistoryMapActivity.this);
                    builder.setTitle(R.string.connection_off_title);
                    builder.setMessage(R.string.connection_off_message);
                    builder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            get_again = true;
                            Intent intent = new Intent(Settings.ACTION_SETTINGS);
                            startActivity(intent);
                        }
                    });
                    builder.create().show();
                }
                return true;
            case R.id.mMonth: //Historial del último mes
                manager = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
                info = manager.getActiveNetworkInfo();
                if ((info != null) && info.isConnected()) {
                    task = new GetHistoryPositionAsyncTask();
                    task.setParent(ChildHistoryMapActivity.this);
                    String user_username = prefs.getString("active_user_username","No Username");
                    String user_pass = prefs.getString("active_user_password","No Password").replace("$","%24");
                    String child_username = prefs.getString("active_child_username","No Username");
                    String period = "lm";
                    String[] params = {user_username, user_pass, child_username, period};
                    task.execute(params);
                    pb.setVisibility(View.VISIBLE);
                } else {
                    AlertDialog.Builder builder = new AlertDialog.Builder(ChildHistoryMapActivity.this);
                    builder.setTitle(R.string.connection_off_title);
                    builder.setMessage(R.string.connection_off_message);
                    builder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            get_again = true;
                            Intent intent = new Intent(Settings.ACTION_SETTINGS);
                            startActivity(intent);
                        }
                    });
                    builder.create().show();
                }
                return true;
        }
        return super.onOptionsItemSelected(item); //Flecha hacia atrás
    }

    public void setListPos(ArrayList<HashMap<String, String>> result){
        positions.clear();
        positions.addAll(result);
        adapter.notifyDataSetChanged();
        //positions = result;
        //SimpleAdapter adapter = new SimpleAdapter(this, positions, R.layout.label_history_children, new String[]{"street", "province", "date"}, new int[]{R.id.street, R.id.city_country, R.id.date});
        //lv.setAdapter(adapter);
        pb.setVisibility(View.INVISIBLE);
    }

    public void setInfo(){
        Toast.makeText(this,R.string.no_history,Toast.LENGTH_SHORT).show();
        positions.clear();
        adapter.notifyDataSetChanged();
        pb.setVisibility(View.INVISIBLE);
    }

    @Override
    protected void onResume() {
        if (get_again) {
            manager = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
            info = manager.getActiveNetworkInfo();
            if ((info != null) && info.isConnected()) {
                task = new GetHistoryPositionAsyncTask();
                task.setParent(ChildHistoryMapActivity.this);
                String user_username = prefs.getString("active_user_username","No Username");
                String user_pass = prefs.getString("active_user_password","No Password").replace("$","%24");
                String child_username = prefs.getString("active_child_username","No Username");
                String period = "ld";
                String[] params = {user_username, user_pass, child_username, period};
                task.execute(params);
                pb.setVisibility(View.VISIBLE);
            } else {
                AlertDialog.Builder builder = new AlertDialog.Builder(ChildHistoryMapActivity.this);
                builder.setTitle(R.string.connection_off_title);
                builder.setMessage(R.string.connection_off_message);
                builder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        get_again = true;
                        Intent intent = new Intent(Settings.ACTION_SETTINGS);
                        startActivity(intent);
                    }
                });
                builder.create().show();
            }
        }

        if (get_again2) {
            manager = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
            info = manager.getActiveNetworkInfo();
            if ((info != null) && info.isConnected()) {
                task = new GetHistoryPositionAsyncTask();
                task.setParent(ChildHistoryMapActivity.this);
                String user_username = prefs.getString("active_user_username","No Username");
                String user_pass = prefs.getString("active_user_password","No Password").replace("$","%24");
                String child_username = prefs.getString("active_child_username","No Username");
                String period = "lw";
                String[] params = {user_username, user_pass, child_username, period};
                task.execute(params);
                pb.setVisibility(View.VISIBLE);
            } else {
                AlertDialog.Builder builder = new AlertDialog.Builder(ChildHistoryMapActivity.this);
                builder.setTitle(R.string.connection_off_title);
                builder.setMessage(R.string.connection_off_message);
                builder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        get_again = true;
                        Intent intent = new Intent(Settings.ACTION_SETTINGS);
                        startActivity(intent);
                    }
                });
                builder.create().show();
            }
        }

        if (get_again3) {
            manager = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
            info = manager.getActiveNetworkInfo();
            if ((info != null) && info.isConnected()) {
                task = new GetHistoryPositionAsyncTask();
                task.setParent(ChildHistoryMapActivity.this);
                String user_username = prefs.getString("active_user_username","No Username");
                String user_pass = prefs.getString("active_user_password","No Password").replace("$","%24");
                String child_username = prefs.getString("active_child_username","No Username");
                String period = "lm";
                String[] params = {user_username, user_pass, child_username, period};
                task.execute(params);
                pb.setVisibility(View.VISIBLE);
            } else {
                AlertDialog.Builder builder = new AlertDialog.Builder(ChildHistoryMapActivity.this);
                builder.setTitle(R.string.connection_off_title);
                builder.setMessage(R.string.connection_off_message);
                builder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        get_again = true;
                        Intent intent = new Intent(Settings.ACTION_SETTINGS);
                        startActivity(intent);
                    }
                });
                builder.create().show();
            }
        }
        super.onResume();
    }
}
