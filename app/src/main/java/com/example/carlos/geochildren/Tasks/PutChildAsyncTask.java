package com.example.carlos.geochildren.Tasks;

import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.example.carlos.geochildren.Activities.MainActivity;
import com.example.carlos.geochildren.Activities.RegisterActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by efrencamarasacarreres on 18/4/16.
 */
public class PutChildAsyncTask extends AsyncTask<String,Void,ArrayList<HashMap<String, Object>>> {

    MainActivity parent;
    String username;
    String password;
    String child_name;
    String task = "PutDeleteChildAsyncTask";
    ArrayList<HashMap<String, Object>> result;
    HashMap<String,Object> item;
    Integer code;

    public static String convertStreamToString(java.io.InputStream is){
        java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
        return s.hasNext() ? s.next():"";
    }

    public void setParent(MainActivity parent){
        this.parent = parent;
    }

    @Override
    protected ArrayList<HashMap<String, Object>> doInBackground(String... params) {
        username = params[0];
        password = params[1];
        child_name = params[2];
        result = new ArrayList<>();
        Log.i("111111111122222",child_name.toString());
        InputStreamReader isr = null;
        HttpURLConnection connection = null;
        /*
        Uri.Builder builder = new Uri.Builder();
        builder.scheme("http");
        builder.authority("213.201.97.154:8081");
        builder.appendPath("gc");
        builder.appendPath("v1");
        builder.appendPath("users");
        builder.appendPath("related");
        builder.appendPath("add");
        builder.appendPath(username);
        builder.appendPath(password);
        builder.appendPath(child_name);
        */


        try {
            //URL url = new URL(builder.build().toString());
            URL url = new URL("http://213.201.97.154:8081/gc/v1/users/related/add/"+username+"/"+password+"/"+child_name);
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("PUT");
            connection.setDoInput(true);
            if (connection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                isr = new InputStreamReader(connection.getInputStream(), "UTF-8");


                String response = convertStreamToString(connection.getInputStream());
                JSONObject object = null;
                code = -1;
                String message = null;
                try {
                    object = new JSONObject(response);
                    code = object.getInt("code");
                    Log.i("code",""+code);
                    JSONObject data = object.getJSONObject("data");
                    message = object.getString("message");

                    if(code == 3){
                        Log.i(task,"El username no existe");
                    }

                    if(code == 4){
                        Log.i(task,"La contraseña no es correcta");
                    }

                    if(code == 0) {
                        JSONArray array = data.names();
                        Log.i("list",array.toString());
                        Log.i("length",""+array.length());
                        for(int i=0;i<array.length();i++){
                            String child_username = array.getString(i);
                            Log.i("name",child_username);
                            JSONObject child_info = data.getJSONObject(child_username);
                            item = new HashMap<>();
                            item.put("name",child_info.getString("name"));
                            Log.i("name",child_info.getString("name"));
                            item.put("username",child_username);
                            item.put("sex",child_info.getString("sex"));
                            Log.i("sex",child_info.getString("sex"));
                            result.add(item);
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        finally {
            try {
                isr.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            connection.disconnect();
        }
        Log.i("result", result.toString());
        return result;
    }

    @Override
    protected void onPostExecute(ArrayList<HashMap<String, Object>> result) {
        if(!result.isEmpty()){
            parent.setListChildren(result);
        }
        else{
            parent.setInfoAddChild(code);

        }
        super.onPostExecute(result);
    }
}
