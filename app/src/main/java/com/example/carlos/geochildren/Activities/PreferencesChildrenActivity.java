package com.example.carlos.geochildren.Activities;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.preference.PreferenceManager;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.Switch;

import com.example.carlos.geochildren.R;
import com.example.carlos.geochildren.Tasks.PutPeriodAsyncTask;

public class PreferencesChildrenActivity extends AppCompatActivity {

    SharedPreferences prefs;
    SharedPreferences.Editor editor;
    Switch sos_not;
    Spinner period_not;
    PutPeriodAsyncTask task4;

    //Periodos notificaciones sos indexados por la posición que te devuelve el spinner
    int [] periods = {5,10,15,30,45,60};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preferences_children);

        prefs = PreferenceManager.getDefaultSharedPreferences(this);
        editor = prefs.edit();

        String name = prefs.getString("active_child_name","No Name");
        getSupportActionBar().setTitle(getString(R.string.prefs_child)+" "+name);

        //Recuperamos el estado del switch
        sos_not = (Switch) findViewById(R.id.sos_notifications);
        //Como hay diversos child, utilizaremos una variable para cada uno usando su username
        sos_not.setChecked(prefs.getBoolean("sos_notifications_child_"+prefs.getString("active_child_username","No Username"),true));

        //Recuperamos el perido de las notificaciones sos para cada child
        period_not = (Spinner) findViewById(R.id.spinner_child_pref);
        period_not.setSelection(prefs.getInt("sos_period_child_"+prefs.getString("active_child_username","No Username"),3)); //Periodo 30 minutos por defecto

        period_not.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                int period = periods[position];
                task4 = new PutPeriodAsyncTask();
                task4.setParent(PreferencesChildrenActivity.this);
                String username_delete_child = prefs.getString("active_user_username", "No Name");
                String password_delete_child = prefs.getString("active_user_password", "No Password").replace("$", "%24");
                String minutes =String.valueOf(period);
                String name_child = prefs.getString("active_child_username","No Username");
                String[] params = {username_delete_child, password_delete_child,minutes, name_child};
                task4.execute(params);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public void button_function(View v){

    }

    protected void onPause() {
        //Guargar el valor del switch
        sos_not = (Switch) findViewById(R.id.sos_notifications);
        //Como hay diversos child utilizaremos una variable para cada uno usando su username
        editor.putBoolean("sos_notifications_child_"+prefs.getString("active_child_username","No Username"),sos_not.isChecked());
        //Guardamos el perido (posicion) de las notificaciones sos para cada child
        editor.putInt("sos_period_child_"+prefs.getString("active_child_username","No Username"),period_not.getSelectedItemPosition());
        //Guardamos el perido (value) de las notificaciones sos para cada child (Para el servidor)
        editor.putInt("sos_value_period_child_"+prefs.getString("active_child_username","No Username"),periods[period_not.getSelectedItemPosition()]);
        editor.apply();
        super.onPause();
    }


}
