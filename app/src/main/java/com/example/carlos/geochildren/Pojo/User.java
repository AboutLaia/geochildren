package com.example.carlos.geochildren.Pojo;

/**
 * Created by efrencamarasacarreres on 18/4/16.
 */
public class User {

    String username;
    String name;
    String surnames;
    String sex;
    String rol;
    String password;

    public User() {
        this.username = "No Username";
        this.name = "No Name";
        this.password = "No Password";
        this.surnames = "No Surnames";
        this.sex = "No Sex";
        this.rol = "No Rol";
    }

    public User(String username, String name, String password,String surnames, String rol, String sex) {
        this.username = username;
        this.name = name;
        this.password = password;
        this.surnames = surnames;
        this.rol = rol;
        this.sex = sex;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getSurnames() {
        return surnames;
    }

    public void setSurnames(String surnames) {
        this.surnames = surnames;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getRol() {
        return rol;
    }

    public void setRol(String rol) {
        this.rol = rol;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
