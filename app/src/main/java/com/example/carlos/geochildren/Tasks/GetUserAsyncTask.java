package com.example.carlos.geochildren.Tasks;

import android.net.Uri;
import android.os.AsyncTask;
import android.support.v4.app.INotificationSideChannel;
import android.util.Log;

import com.example.carlos.geochildren.Activities.LogInActivity;
import com.example.carlos.geochildren.Activities.RegisterActivity;
import com.example.carlos.geochildren.Pojo.Position;
import com.example.carlos.geochildren.Pojo.User;
import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.HashMap;

/**
 * Created by efrencamarasacarreres on 18/4/16.
 */
public class GetUserAsyncTask extends AsyncTask<String, Void, User> {
    //Para obtener un usuario
    LogInActivity parent;
    User user;
    String username;
    String password;
    String task = "GetUserAsyncTask";
    Integer code;

    public static String convertStreamToString(java.io.InputStream is) {
        java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
        return s.hasNext() ? s.next() : "";
    }

    public void setParent(LogInActivity parent) {
        this.parent = parent;
    }

    @Override
    protected User doInBackground(String... params) {
        username = params[0];
        password = params[1];


        InputStreamReader isr = null;
        HttpURLConnection connection = null;
        /*
        Uri.Builder builder = new Uri.Builder();
        builder.scheme("http");
        builder.authority("213.201.97.154:8081");
        builder.appendPath("gc");
        builder.appendPath("v1");
        builder.appendPath("users");
        builder.appendPath("signin");
        builder.appendPath(username);
        builder.appendPath(password);

        */
        try {
            //URL url = new URL(builder.build().toString());
            URL url = new URL("http://213.201.97.154:8081/gc/v1/users/signin/"+username+"/"+password);
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.setDoInput(true);
            if (connection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                isr = new InputStreamReader(connection.getInputStream());

                String response = convertStreamToString(connection.getInputStream());
                JSONObject object = null;
                code = -1;
                String message = null;
                try {
                    object = new JSONObject(response);
                    JSONObject data = object.getJSONObject("data");
                    code = object.getInt("code");
                    message = object.getString("message");

                    if (code == 3) {
                        Log.i(task, "El username no existe");
                    }

                    if (code == 4) {
                        Log.i(task, "La contraseña no es correcta");
                    }

                    if (code == 0) {
                        user = new User(data.getString("username"),data.getString("name"),data.getString("password"),
                                data.getString("surname"),data.getString("rol"),data.getString("sex"));
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } /*finally {
            try {
                isr.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            connection.disconnect();
        }*/
        return user;
    }

    @Override
    protected void onPostExecute(User user) {
        if(user != null){
            parent.setUser(user);
        }
        else{
            parent.setInfo(code);
        }
        super.onPostExecute(user);
    }
}