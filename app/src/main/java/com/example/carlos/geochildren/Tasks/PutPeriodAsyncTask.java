package com.example.carlos.geochildren.Tasks;

import android.os.AsyncTask;
import android.util.Log;

import com.example.carlos.geochildren.Activities.MainActivity;
import com.example.carlos.geochildren.Activities.PreferencesChildrenActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by efrencamarasacarreres on 23/4/16.id
 */
public class PutPeriodAsyncTask extends AsyncTask<String,Void,Void> {
    PreferencesChildrenActivity parent;
    String username;
    String password;
    String minutes;
    String child;
    Integer result;
    String task = "PutPeriodAsyncTask";

    public static String convertStreamToString(java.io.InputStream is) {
        java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
        return s.hasNext() ? s.next() : "";
    }

    public void setParent(PreferencesChildrenActivity parent) {
        this.parent = parent;
    }

    protected Void doInBackground(String... params) {
        username = params[0];
        password = params[1];
        minutes = params[2];
        child = params[3];
        InputStreamReader isr = null;
        HttpURLConnection connection = null;

        try {
            //URL url = new URL(builder.build().toString());
            URL url = new URL("http://213.201.97.154:8081/gc/v1/prefs/" + username + "/" + password + "/" + minutes + "/" + child);
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("PUT");
            connection.setDoInput(true);
            if (connection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                isr = new InputStreamReader(connection.getInputStream(), "UTF-8");


                String response = convertStreamToString(connection.getInputStream());
                JSONObject object = null;
                Integer code = -1;
                try {
                    object = new JSONObject(response);
                    code = object.getInt("code");
                    Log.i("code", "" + code);
                    JSONObject data = object.getJSONObject("data");


                    if (code == 3) {
                        Log.i(task, "El username no existe");
                    }

                    if (code == 4) {
                        Log.i(task, "La contraseña no es correcta");
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                isr.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            connection.disconnect();
        }
        return null;
    }
}

