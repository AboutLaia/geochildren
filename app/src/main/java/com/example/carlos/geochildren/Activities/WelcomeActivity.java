package com.example.carlos.geochildren.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.example.carlos.geochildren.R;

public class WelcomeActivity extends AppCompatActivity {

    Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
    }

    public void buttons_welcome(View v){
        switch (v.getId()){
            case R.id.btLogIn:
                intent = new Intent(this,LogInWelcomeActivity.class);
                break;
            case R.id.btRegister:
                intent = new Intent(this,RegisterWelcomeActivity.class);
                break;
        }
        if(intent != null){
            this.finish();
            startActivity(intent);
        }
    }
}
