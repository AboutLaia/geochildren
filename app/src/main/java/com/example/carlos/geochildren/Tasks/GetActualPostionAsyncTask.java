package com.example.carlos.geochildren.Tasks;

import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;

import com.example.carlos.geochildren.Activities.ChildMapActivity;
import com.example.carlos.geochildren.Activities.MenuChildrenActivity;
import com.example.carlos.geochildren.Pojo.Position;
import com.example.carlos.geochildren.Pojo.User;
import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

/**
 * Created by efrencamarasacarreres on 18/4/16.
 */
public class GetActualPostionAsyncTask extends AsyncTask<String,Void,Position> {
    //Para obtener la ubicación actual

    ChildMapActivity parent;
    String child_username,parent_username,parent_pass;
    Position position_children;
    Integer code;
    String task = "GetActualPositionAsyncTask";

    public static String convertStreamToString(java.io.InputStream is) {
        java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
        return s.hasNext() ? s.next() : "";
    }

    public void setParent(ChildMapActivity parent) {
        this.parent = parent;
    }

    @Override
    protected Position doInBackground(String... params) {
        parent_username = params[0];
        parent_pass = params[1];
        child_username = params[2];

        InputStreamReader isr = null;
        HttpURLConnection connection = null;
        /*
        Uri.Builder builder = new Uri.Builder();
        builder.scheme("http");
        builder.authority("213.201.97.154:8081");
        builder.appendPath("ps");
        builder.appendPath("v1");
        builder.appendPath("users");
        builder.appendPath("position");
        builder.appendPath(child_username);
        */

        try {
            //URL url = new URL(builder.build().toString());
            URL url = new URL("http://213.201.97.154:8081/gc/v1/locations/"+parent_username+"/"+parent_pass+"/"+child_username+"?d=ll");
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.setDoInput(true);
            if (connection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                isr = new InputStreamReader(connection.getInputStream());

                String response = convertStreamToString(connection.getInputStream());
                Log.i("response",response);
                JSONObject object = null;
                code = -1;
                String message = null;
                try {
                    object = new JSONObject(response);
                    code = object.getInt("code");
                    JSONObject data = object.getJSONObject("data");
                    message = object.getString("message");

                    if (code == 3) {
                        Log.i(task, "El username no existe");
                    }

                    if (code == 4) {
                        Log.i(task, "La contraseña no es correcta");
                    }

                    if (code == 0) {
                        position_children = new Position(data.getString("longitude"),data.getString("latitude"),
                                data.getString("street"),data.getString("province"),data.getString("updatedAt"));
                    }
                } catch (JsonSyntaxException | JsonIOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        finally {
            try {
                isr.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            connection.disconnect();
        }

        return position_children;
    }

    @Override
    protected void onPostExecute(Position position) {
        if(position != null){
            parent.setActualPos(position);
        }
        else{
            parent.setNoPos();
        }
        super.onPostExecute(position);
    }
}
