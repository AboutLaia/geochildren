package com.example.carlos.geochildren.Tasks;

import android.os.AsyncTask;
import android.util.Log;

import com.example.carlos.geochildren.Activities.ChildHistoryMapActivity;
import com.example.carlos.geochildren.Pojo.Position;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by efrencamarasacarreres on 18/4/16.
 */
public class GetHistoryPositionAsyncTask extends AsyncTask<String,Void,ArrayList<HashMap<String, String>>> {
    //Para obtener el historial de posiciones
    ChildHistoryMapActivity parent;
    String parent_username, parent_pass, child_username, period;
    ArrayList<HashMap<String, String>> result;
    HashMap<String,String> item;
    ArrayList<Position> positions;
    Integer code;

    String task = "GetHistoryPositionAsyncTask";

    public static String convertStreamToString(java.io.InputStream is){
        java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
        return s.hasNext() ? s.next():"";
    }

    public void setParent(ChildHistoryMapActivity parent) {
        this.parent = parent;
    }

    @Override
    protected ArrayList<HashMap<String,String>> doInBackground(String... params) {
        parent_username = params[0];
        parent_pass = params[1];
        child_username = params[2];
        period = params[3];
        result = new ArrayList<>();

        InputStreamReader isr = null;
        HttpURLConnection connection = null;
        /*
        Uri.Builder builder = new Uri.Builder();
        builder.scheme("http");
        builder.authority("213.201.97.154:8081");
        builder.appendPath("ps");
        builder.appendPath("v1");
        builder.appendPath("users");
        builder.appendPath("history");
        builder.appendPath(child_username);
        */

        try {
            //URL url = new URL(builder.build().toString());
            URL url = new URL("http://213.201.97.154:8081/gc/v1/locations/"+parent_username+"/"+parent_pass+"/"+child_username+"?d="+period);
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.setDoInput(true);
            if (connection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                isr = new InputStreamReader(connection.getInputStream());

                String response = convertStreamToString(connection.getInputStream());
                JSONObject object = null;
                code = -1;
                String message = null;
                try {
                    object = new JSONObject(response);
                    code = object.getInt("code");
                    JSONArray data = object.getJSONArray("data");
                    Log.i("array",data.toString());
                    message = object.getString("message");
                    if(code == 3){
                        Log.i(task,"El username no existe");
                    }

                    if(code == 4){
                        Log.i(task,"La contraseña no es correcta");
                    }

                    if(code == 0){
                        for(int i=0;i<data.length();i++){
                            JSONObject obj = data.getJSONObject(i);
                            Log.i("obj",obj.toString());
                            item = new HashMap<>();
                            item.put("street",obj.getString("street"));
                            Log.i("street",obj.getString("street"));
                            item.put("province",obj.getString("province"));
                            Log.i("province",obj.getString("province"));
                            String [] ar = obj.getString("updatedAt").split("T");
                            Log.i("ar",ar[0]+" "+ar[1]);
                            String [] ar2 = ar[1].split("\\.");
                            Log.i("ar2",ar2[0]);
                            item.put("date",ar[0]+" "+ar2[0]+" (GMT)");
                            Log.i("date",ar[0]+" "+ar2[0]+" (GMT)");
                            result.add(item);
                        }
                    }
                } catch (JsonSyntaxException | JsonIOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        finally {
            try {
                isr.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            connection.disconnect();
        }

        return result;
    }

    @Override
    protected void onPostExecute(ArrayList<HashMap<String, String>> result) {
        if(!result.isEmpty()){
            parent.setListPos(result);
        }
        else{
            parent.setInfo();
        }
        super.onPostExecute(result);
    }
}
